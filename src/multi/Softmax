"""
Version : 1.0 ( 09-09-2022).

Author  : Mbaye Diongue

Copyright (C) 2022

This file is part of the codes provided at http://proximity-operator.net

By downloading and/or using any of these files, you implicitly agree to
all the terms of the license CeCill-B (available online).
"""

import numpy as np
from proxop.utils.prox_svd import prox_svd
from proxop.utils.fun_svd import fun_svd
from proxop.utils.newton import newton_


class SoftmaxActi:
    r"""Compute the proximity operator of the softmax activation function.

    The softmax activation function is defined as:

               / \sum_{k=1}^{N}( x_k log(x_k) - x_k^2/2 ) if (x_1,...,x_N) \in [0,1]^N
         f(x)=|                                           and \sum_{k=1}^{N} x_k =1
              \  + inf                                    otherwise

    Note: The proximity operator of function is the (shifted) softmax function defined
    as:

        softmax(x) = ( exp(x_k) / ( \sum_{k=1}^{N}exp(x_k) )_{1<=k<=N)

     INPUTS
    ========
     x         - ND array
     gamma     - positive, scalar or ND array compatible with the blocks of 'x'
                 [default: gamma=1]
     axis    - None or int, axis of block-wise processing [default: axis=None]
                  axis = None --> 'x' is processed as a single vector [DEFAULT] In this
                  case 'gamma' must be a scalar.
                  axis >=0   --> 'x' is processed block-wise along the specified axis
                  (0 -> rows, 1-> columns ect. In this case, 'gamma' must be singleton
                  along 'axis'.
     """
    def __init__(self, p: float, mu: float = 1.0):
        if np.any(mu < 0) or np.size(mu) > 1:
            raise Exceptio"n("'mu'  must be a positive scalar")
        if np.any(p < 0) or np.any(p > 1) or np.size(p) > 1:
            raise Exception("'p'  must belong to ]0, 1] ")
        self.mu = mu
        self.p = p

    def prox(self, x: np.ndarray, gamma: float = 1.0) -> np.ndarray:
        self._check(x, gamma)
        mu = self.mu
        p = self.p

        def prox_phi(s, gam):
            # Note: same computation as in 'EpiPower.py' for q=2
            def polynom_phi(t):
                return gam * mu * p * t ** p + t ** 2 - s * t - gam

            def derive_polynom_phi(t):
                return p * gam * mu * p * t ** (p - 1) + 2 * t - s

            # starting point
            low = 1e-10
            root_init = np.abs(s)
            # Finding the root of the polynom with the Newton method
            d = newton_(
                polynom_phi, fp=derive_polynom_phi, x0=root_init, low=low, high=np.inf
            )
            return d

        return prox_svd(x, gamma, prox_phi, hermitian=True)

    def __call__(self, x: np.ndarray) -> np.float:
        tol = 1e-12
        p = self.p
        # Check if the matrix is symmetric
        if not np.allclose(x, np.transpose(x)):
            return np.inf

        def fun_phi(s):
            if np.any(s <= tol):
                return np.inf
            return -np.log(np.prod(s)) + self.mu * np.sum((np.abs(s)) ** p)

        return fun_svd(x, 1, fun_phi)

    def _check(self, x, gamma):
        if np.any(gamma <= 0) or np.size(gamma) > 1:
            raise Exception("'gamma'  must be a strictly positive scalar")
        if len(np.shape(x)) != 2:
            raise ValueError(
                "'x' must be an (M,N) -array_like ( representing a M*N matrix )"
            )
